const path = require('path');

module.exports = (eZConfig, eZConfigManager) => {
    eZConfigManager.add({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-js',
        newItems: [
            path.resolve(__dirname, '../public/js/alloyeditor/buttons/special-characters.js'),
            path.resolve(__dirname, '../public/js/alloyeditor/externals.js'),
        ]
    });
};