import React from 'react';
import AlloyEditor from 'alloyeditor';
import EzButton
    from './../../../../../../../../ezsystems/ezplatform-richtext/src/bundle/Resources/public/js/OnlineEditor/buttons/base/ez-button';

class BtnSpecialCharacters extends EzButton {
    static get key() {
        return 'specialcharacters';
    }

    editSource() {
        const editor = this.props.editor.get('nativeEditor');
        editor.execCommand('specialchar');
    }

    render() {
        return (
            <button
                className="ae-button ez-btn-ae ez-btn-ae--source"
                onClick={this.editSource.bind(this)}
                tabIndex={this.props.tabIndex}
                title={Translator.trans('button.label', {}, 'special_characters')}>
                <svg className="ez-icon ez-btn-ae__icon">
                    <use xlinkHref="/bundles/ezplatformalloyeditorspecialcharacters/img/icons.svg#special-characters" />
                </svg>
            </button>
        );
    }
}

AlloyEditor.Buttons[BtnSpecialCharacters.key] = AlloyEditor.BtnSpecialCharacters = BtnSpecialCharacters;