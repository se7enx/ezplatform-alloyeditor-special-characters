<?php

namespace ContextualCode\EzPlatformAlloyEditorSpecialCharacters;

use ContextualCode\EzPlatformAlloyEditorSpecialCharacters\DependencyInjection\EzPlatformAlloyEditorSpecialCharactersExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EzPlatformAlloyEditorSpecialCharactersBundle extends Bundle
{
    protected $extension;

    public function getContainerExtension(): EzPlatformAlloyEditorSpecialCharactersExtension
    {
        if (null === $this->extension) {
            $this->extension = new EzPlatformAlloyEditorSpecialCharactersExtension();
        }

        return $this->extension;
    }
}
