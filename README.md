# eZ Platform AlloyEditor Special Characters

This bundle allows inserting special characters in eZ Platform Online Editor.
 
![edit_source_modal](https://gitlab.com/contextualcode/ezplatform-alloyeditor-special-characters/raw/master/doc/images/usage_example.png)
 
## Installation

1. Require via composer
    ```bash
    composer require contextualcode/ezplatform-alloyeditor-special-characters
    ```

2. Clear browser caches and enjoy!

## Using CKEditor add-on in eZ Platform UI

This bundle is a good showcase for using CK Editor plugin in eZ Platform. It relies on [Special Characters](https://ckeditor.com/cke4/addon/specialchar) CKEditor add-on.

If you want to make a similar bundle for any other CKEditor add-on, just follow these steps: 
1. Add CKEditor add-on to your source: [src/bundle/Resources/public/js/alloyeditor/plugins/specialchar](https://gitlab.com/contextualcode/ezplatform-alloyeditor-special-characters/-/tree/b58f4ccb61d6f64c41794398220d87edb5fd2f60/src/Resources/public/js/alloyeditor/plugins/specialchar).
2. Create an external source for previously added add-on: [src/bundle/Resources/public/js/alloyeditor/externals.js](https://gitlab.com/contextualcode/ezplatform-alloyeditor-special-characters/-/blob/b58f4ccb61d6f64c41794398220d87edb5fd2f60/src/Resources/public/js/alloyeditor/externals.js)
3. Add plugin external source to encore configuration: [src/bundle/Resources/encore/ez.config.manager.js](https://gitlab.com/contextualcode/ezplatform-alloyeditor-special-characters/-/blob/b58f4ccb61d6f64c41794398220d87edb5fd2f60/src/Resources/encore/ez.config.manager.js)
4. Register added plugin in eZ Platform Admin UI: [src/bundle/Resources/config/ezrichtext.yaml](https://gitlab.com/contextualcode/ezplatform-alloyeditor-special-characters/-/blob/b58f4ccb61d6f64c41794398220d87edb5fd2f60/src/Resources/config/ezrichtext.yaml#L2)
5. In most cases, CKEditor plugins have their buttons, but they require minor CSS fixes to be nice and shiny in eZ Platform Admin UI. Alternatively, you can create a new button: [src/bundle/Resources/public/js/alloyeditor/buttons/special-characters.js](https://gitlab.com/contextualcode/ezplatform-alloyeditor-special-characters/-/blob/b58f4ccb61d6f64c41794398220d87edb5fd2f60/src/Resources/public/js/alloyeditor/buttons/special-characters.js)
6. Enable the button from the previous step in desired Online Editor toolbars: [src/bundle/Resources/config/ezpublish.yaml](https://gitlab.com/contextualcode/ezplatform-alloyeditor-special-characters/-/blob/b58f4ccb61d6f64c41794398220d87edb5fd2f60/src/Resources/config/ezpublish.yaml)

You can find more details at [Extending eZ Platform Online Editor](https://www.contextualcode.com/Blog/Extending-eZ-Platform-Online-Editor) blog post.